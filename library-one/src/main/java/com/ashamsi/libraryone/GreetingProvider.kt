package com.ashamsi.libraryone

import android.util.Log

class GreetingProvider {
    fun greet(tag: String, greetMessage: String?) {
       if (greetMessage.isNullOrEmpty()) Log.d(tag, "Hello There!") else Log.d(tag, greetMessage)
    }
}