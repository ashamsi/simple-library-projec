package com.ashamsi.simplelibraryproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        MainActivity::class.simpleName?.let { com.ashamsi.libraryone.GreetingProvider().greet(it, "Hello from client code!") }
        MainActivity::class.simpleName?.let { com.ashamsi.librarytwo.GreetingProvider().greet(it, "Hello from client code!") }
    }
}